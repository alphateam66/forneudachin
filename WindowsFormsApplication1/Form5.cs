﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _24._09._2014
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            int n = 0;
            foreach(string s in richTextBox1.Lines)
            {
                if (s != "")
                    n++;
            }
            textBox1.Text = n.ToString();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}
