﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _24._09._2014
{
    public partial class Form2 : Form
    {
        Graphics kist;
        Bitmap pix;
        Form3 fm3;
        Pen pen;
        Brush braG, back, braO, braR, braB;
        public Form2()
        {
            InitializeComponent();
            fm3 = new Form3();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            kist = this.CreateGraphics();
            pen = new Pen(Color.Orange, 3);
            braG = new SolidBrush(Color.Green);
            back = new SolidBrush(BackColor);
            braO = new SolidBrush(Color.Orange);
            braR = new SolidBrush(Color.Red);
            braB = new SolidBrush(Color.Blue);
            pix = new Bitmap(600, 600);
            kist.FillEllipse(braO, 20, 40, 40, 40);
            pictureBox1.Visible = false;
            dataGridView1.ColumnCount = 3;
            dataGridView1.RowCount = 3;
            dataGridView1.AutoSize = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                for (int j = 0; j < dataGridView1.RowCount; j++)
                {
                    dataGridView1.Rows[j].Cells[i].Value = "Коорд. " + i.ToString() + ", " + j.ToString();
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                kist.FillEllipse(back, 20, 40, 40, 40);
                kist.FillEllipse(braG, 20, 40, 40, 40);
            }
            else
            {
                kist.FillEllipse(back, 20, 40, 40, 40);
                kist.FillEllipse(braO, 20, 40, 40, 40);
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                pictureBox1.Visible = true;
            }
            else
            {
                pictureBox1.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Проверка кнопки справка");
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            fm3.ShowDialog();
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            panel2.VerticalScroll.Value = vScrollBar1.Value;
        }

        private void команда6ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[0].Value = команда6ToolStripMenuItem.Text;
        }

        private void команда1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[0].Value = команда1ToolStripMenuItem.Text;
        }

        private void команда2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[1].Value = команда2ToolStripMenuItem.Text;
        }

        private void команда3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[2].Value = команда3ToolStripMenuItem.Text;
        }

        private void команда4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[0].Value = команда4ToolStripMenuItem.Text;
        }

        private void команда5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[1].Value = команда5ToolStripMenuItem.Text;
        }

        private void команда6ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[2].Value = команда6ToolStripMenuItem.Text;
        }

        private void команда8ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[1].Value = команда6ToolStripMenuItem.Text;
        }

        private void команда9ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[2].Value = команда6ToolStripMenuItem.Text;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void drawTriangle(int startX, int startY, int len1, int len2)
        {
            kist.DrawLine(pen, startX, startY, startX, startY+len2);
            kist.DrawLine(pen, startX, startY + len2, startX+len1, startY + len2);
            kist.DrawLine(pen, startX, startY, startX+len1, startY + len2);
        }

        private void Shape_Click(object sender, EventArgs e)
        {
            kist.Clear(this.BackColor);
            Random rnd = new Random();
            int curElem = 0, position = 10, len = 1;
            for (int i = 0; i < 10; i++ )
            {
                curElem = rnd.Next(0,3);
                len = rnd.Next(25, 51);
                if (curElem == 0)
                {
                    kist.FillEllipse(braG, position, 350, len, rnd.Next(25, 51));
                }
                if (curElem == 1)
                {
                    kist.FillRectangle(braB, position, 350, len, rnd.Next(25, 51));
                }
                if (curElem == 2)
                {
                    drawTriangle(position, 350, len, rnd.Next(25, 51));
                }
                position += len + 5;
            }
            /*kist.FillEllipse(braG, 30, 350, 50, 50);
            kist.FillRectangle(braB, 100, 350, 50, 50);
            kist.DrawLine(pen, 160, 350, 160, 400);
            kist.DrawLine(pen, 160, 400, 210, 400);
            kist.DrawLine(pen, 160, 350, 210, 400);
            kist.FillRectangle(braG, 215, 350, 45, 45);*/
        }
    
        private void команда1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[0].Value = команда1ToolStripMenuItem.Text;
        }

        private void команда2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[1].Value = команда2ToolStripMenuItem.Text;
        }

        private void команда3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[2].Value = команда3ToolStripMenuItem.Text;
        }

        private void команда4ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[0].Value = команда4ToolStripMenuItem.Text;
        }

        private void команда5ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[1].Value = команда5ToolStripMenuItem.Text;
        }

        private void команда6ToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            dataGridView1.Rows[1].Cells[2].Value = команда6ToolStripMenuItem.Text;
        }

        private void команда7ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[0].Value = команда7ToolStripMenuItem.Text;
        }

        private void команда8ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[1].Value = команда8ToolStripMenuItem.Text;
        }

        private void команда9ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[2].Cells[2].Value = команда9ToolStripMenuItem.Text;
        }

        private void команда10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 4;
            dataGridView1.Rows[3].Cells[0].Value = команда10ToolStripMenuItem.Text;
        }

        private void команда10ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 4;
            dataGridView1.Rows[3].Cells[0].Value = команда10ToolStripMenuItem.Text;
        }

        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            Color col = pix.GetPixel(e.X, e.Y);
            label2.Text = Convert.ToString(col);
        }
    }
}
