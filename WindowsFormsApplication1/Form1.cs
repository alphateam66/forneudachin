﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _24._09._2014
{
    public partial class Form1 : Form
    {
        Form2 form2;
        Form3 form3;
        int len=0;
        public Form1()
        {
            InitializeComponent();
            form2 = new Form2();
            form3 = new Form3();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.Gray;
            if (len>0)
                for (int i = 0; i < 16; i++)
                    textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            textBox1.Text = textBox1.Text + " Цвет: Серенький";
            trackBar1.Value = 1;
            len++;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.LightBlue;
            if (len > 0)
                for (int i = 0; i < 16; i++)
                    textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            textBox1.Text = textBox1.Text + " Цвет: Синенький";
            trackBar1.Value = 2;
            len++;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.Yellow;
            if (len > 0)
                for (int i = 0; i < 16; i++)
                    textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            textBox1.Text = textBox1.Text + " Цвет:    Желтый";
            trackBar1.Value = 3;
            len++;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.Red;
            if (len > 0)
                for (int i = 0; i < 16; i++)
                    textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            textBox1.Text = textBox1.Text + " Цвет:   Красный";
            trackBar1.Value = 4;
            len++;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (trackBar1.Value == 1)
            {
                radioButton1.Checked = true;
                BackColor = Color.Gray;
            }
            if (trackBar1.Value == 2)
            {
                radioButton2.Checked = true;
                BackColor = Color.LightBlue;

            }
            if (trackBar1.Value == 3)
            {
                radioButton3.Checked = true;
                BackColor = Color.Yellow;
            }
            if (trackBar1.Value == 4)
            {
                radioButton4.Checked = true;
                BackColor = Color.Red;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            richTextBox1.Font = new Font("Arial", 9, FontStyle.Regular);
            radioButton1.Checked = true;
            radioButton5.Checked = true;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            richTextBox1.BackColor = Color.LightBlue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text += textBox1.Text + "\n" + comboBox1.Text + "\nТакт из Listbox № " + (listBox1.SelectedIndex + 1).ToString();
            
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            richTextBox1.BackColor = Color.White;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            if (checkBox1.Checked == true)
            {
                richTextBox1.Font = new Font("Arial", 9, FontStyle.Italic);
                richTextBox1.AppendText("Изменено на Курсив\n");       
            }
            else
            {
                richTextBox1.Font = new Font("Arial", 9);
                richTextBox1.AppendText("Курсив отменён\n");
            }
            if (checkBox2.Checked == true)
            {
                richTextBox1.ForeColor = Color.Purple;
                richTextBox1.AppendText("пурпурный активирован\n");
            }
            else
            {
                richTextBox1.ForeColor = Color.Black;
                richTextBox1.AppendText("пурпурный деактивирован\n");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            form2.ShowDialog();
        }

        private void следующаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form2.ShowDialog();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void поехалиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.button1_Click(sender, e);
        }
       

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void поехалиToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.button1_Click(sender, e);
        }

        private void следующийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.button4_Click(sender, e);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
