﻿﻿namespace _24._09._2014
{
    partial class Form2
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.команда1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда4ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда5ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда6ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда7ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда8ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда9ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.команда10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.stringGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда7ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда9ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.команда10ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Shape = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(407, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(147, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Заполнить таблицу";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(312, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(242, 157);
            this.dataGridView1.TabIndex = 1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.команда1ToolStripMenuItem1,
            this.команда2ToolStripMenuItem1,
            this.команда3ToolStripMenuItem1,
            this.команда4ToolStripMenuItem1,
            this.команда7ToolStripMenuItem1,
            this.команда8ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(129, 136);
            // 
            // команда1ToolStripMenuItem1
            // 
            this.команда1ToolStripMenuItem1.Name = "команда1ToolStripMenuItem1";
            this.команда1ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда1ToolStripMenuItem1.Text = "Команда1";
            this.команда1ToolStripMenuItem1.Click += new System.EventHandler(this.команда1ToolStripMenuItem1_Click);
            // 
            // команда2ToolStripMenuItem1
            // 
            this.команда2ToolStripMenuItem1.Name = "команда2ToolStripMenuItem1";
            this.команда2ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда2ToolStripMenuItem1.Text = "Команда2";
            this.команда2ToolStripMenuItem1.Click += new System.EventHandler(this.команда2ToolStripMenuItem1_Click);
            // 
            // команда3ToolStripMenuItem1
            // 
            this.команда3ToolStripMenuItem1.Name = "команда3ToolStripMenuItem1";
            this.команда3ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда3ToolStripMenuItem1.Text = "Команда3";
            this.команда3ToolStripMenuItem1.Click += new System.EventHandler(this.команда3ToolStripMenuItem1_Click);
            // 
            // команда4ToolStripMenuItem1
            // 
            this.команда4ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.команда5ToolStripMenuItem1,
            this.команда6ToolStripMenuItem1});
            this.команда4ToolStripMenuItem1.Name = "команда4ToolStripMenuItem1";
            this.команда4ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда4ToolStripMenuItem1.Text = "Команда4";
            this.команда4ToolStripMenuItem1.Click += new System.EventHandler(this.команда4ToolStripMenuItem1_Click);
            // 
            // команда5ToolStripMenuItem1
            // 
            this.команда5ToolStripMenuItem1.Name = "команда5ToolStripMenuItem1";
            this.команда5ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда5ToolStripMenuItem1.Text = "Команда5";
            this.команда5ToolStripMenuItem1.Click += new System.EventHandler(this.команда5ToolStripMenuItem1_Click);
            // 
            // команда6ToolStripMenuItem1
            // 
            this.команда6ToolStripMenuItem1.Name = "команда6ToolStripMenuItem1";
            this.команда6ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда6ToolStripMenuItem1.Text = "Команда6";
            this.команда6ToolStripMenuItem1.Click += new System.EventHandler(this.команда6ToolStripMenuItem1_Click_1);
            // 
            // команда7ToolStripMenuItem1
            // 
            this.команда7ToolStripMenuItem1.Name = "команда7ToolStripMenuItem1";
            this.команда7ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда7ToolStripMenuItem1.Text = "Команда7";
            this.команда7ToolStripMenuItem1.Click += new System.EventHandler(this.команда7ToolStripMenuItem1_Click);
            // 
            // команда8ToolStripMenuItem1
            // 
            this.команда8ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.команда9ToolStripMenuItem1});
            this.команда8ToolStripMenuItem1.Name = "команда8ToolStripMenuItem1";
            this.команда8ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда8ToolStripMenuItem1.Text = "Команда8";
            this.команда8ToolStripMenuItem1.Click += new System.EventHandler(this.команда8ToolStripMenuItem1_Click);
            // 
            // команда9ToolStripMenuItem1
            // 
            this.команда9ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.команда10ToolStripMenuItem});
            this.команда9ToolStripMenuItem1.Name = "команда9ToolStripMenuItem1";
            this.команда9ToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.команда9ToolStripMenuItem1.Text = "Команда9";
            this.команда9ToolStripMenuItem1.Click += new System.EventHandler(this.команда9ToolStripMenuItem1_Click);
            // 
            // команда10ToolStripMenuItem
            // 
            this.команда10ToolStripMenuItem.Name = "команда10ToolStripMenuItem";
            this.команда10ToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.команда10ToolStripMenuItem.Text = "Команда10";
            this.команда10ToolStripMenuItem.Click += new System.EventHandler(this.команда10ToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Location = new System.Drawing.Point(73, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 52);
            this.panel1.TabIndex = 2;
            // 
            // checkBox2
            // 
            this.checkBox2.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(86, 15);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(71, 23);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(71, 23);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введите номер телефона:";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(62, 149);
            this.maskedTextBox1.Mask = "(999) 000-00-00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(136, 20);
            this.maskedTextBox1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.vScrollBar1);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(189, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 99);
            this.panel2.TabIndex = 2;
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(224, 0);
            this.vScrollBar1.Maximum = 50;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(20, 131);
            this.vScrollBar1.TabIndex = 2;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(51, 77);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 49);
            this.button3.TabIndex = 1;
            this.button3.Text = "Панель в ScrollBox";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(51, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Справка";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(46, 254);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(96, 42);
            this.button4.TabIndex = 3;
            this.button4.Text = "Следующая";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(46, 209);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(96, 39);
            this.button5.TabIndex = 4;
            this.button5.Text = "Предыдущая";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(244, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.stringGridToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(566, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(108, 22);
            this.Exit.Text = "Выход";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // stringGridToolStripMenuItem
            // 
            this.stringGridToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.команда1ToolStripMenuItem,
            this.команда2ToolStripMenuItem,
            this.команда3ToolStripMenuItem,
            this.команда4ToolStripMenuItem,
            this.команда5ToolStripMenuItem,
            this.команда6ToolStripMenuItem,
            this.команда7ToolStripMenuItem,
            this.команда8ToolStripMenuItem,
            this.команда9ToolStripMenuItem,
            this.команда10ToolStripMenuItem1});
            this.stringGridToolStripMenuItem.Name = "stringGridToolStripMenuItem";
            this.stringGridToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.stringGridToolStripMenuItem.Text = "StringGrid";
            // 
            // команда1ToolStripMenuItem
            // 
            this.команда1ToolStripMenuItem.Name = "команда1ToolStripMenuItem";
            this.команда1ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда1ToolStripMenuItem.Text = "Команда 1";
            this.команда1ToolStripMenuItem.Click += new System.EventHandler(this.команда1ToolStripMenuItem_Click);
            // 
            // команда2ToolStripMenuItem
            // 
            this.команда2ToolStripMenuItem.Name = "команда2ToolStripMenuItem";
            this.команда2ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда2ToolStripMenuItem.Text = "Команда 2";
            this.команда2ToolStripMenuItem.Click += new System.EventHandler(this.команда2ToolStripMenuItem_Click);
            // 
            // команда3ToolStripMenuItem
            // 
            this.команда3ToolStripMenuItem.Name = "команда3ToolStripMenuItem";
            this.команда3ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда3ToolStripMenuItem.Text = "Команда 3";
            this.команда3ToolStripMenuItem.Click += new System.EventHandler(this.команда3ToolStripMenuItem_Click);
            // 
            // команда4ToolStripMenuItem
            // 
            this.команда4ToolStripMenuItem.Name = "команда4ToolStripMenuItem";
            this.команда4ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда4ToolStripMenuItem.Text = "Команда 4";
            this.команда4ToolStripMenuItem.Click += new System.EventHandler(this.команда4ToolStripMenuItem_Click);
            // 
            // команда5ToolStripMenuItem
            // 
            this.команда5ToolStripMenuItem.Name = "команда5ToolStripMenuItem";
            this.команда5ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда5ToolStripMenuItem.Text = "Команда 5";
            this.команда5ToolStripMenuItem.Click += new System.EventHandler(this.команда5ToolStripMenuItem_Click);
            // 
            // команда6ToolStripMenuItem
            // 
            this.команда6ToolStripMenuItem.Name = "команда6ToolStripMenuItem";
            this.команда6ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда6ToolStripMenuItem.Text = "Команда 6";
            this.команда6ToolStripMenuItem.Click += new System.EventHandler(this.команда6ToolStripMenuItem_Click);
            // 
            // команда7ToolStripMenuItem
            // 
            this.команда7ToolStripMenuItem.Name = "команда7ToolStripMenuItem";
            this.команда7ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда7ToolStripMenuItem.Text = "Команда 7";
            this.команда7ToolStripMenuItem.Click += new System.EventHandler(this.команда6ToolStripMenuItem1_Click);
            // 
            // команда8ToolStripMenuItem
            // 
            this.команда8ToolStripMenuItem.Name = "команда8ToolStripMenuItem";
            this.команда8ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда8ToolStripMenuItem.Text = "Команда 8";
            this.команда8ToolStripMenuItem.Click += new System.EventHandler(this.команда8ToolStripMenuItem_Click);
            // 
            // команда9ToolStripMenuItem
            // 
            this.команда9ToolStripMenuItem.Name = "команда9ToolStripMenuItem";
            this.команда9ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.команда9ToolStripMenuItem.Text = "Команда 9";
            this.команда9ToolStripMenuItem.Click += new System.EventHandler(this.команда9ToolStripMenuItem_Click);
            // 
            // команда10ToolStripMenuItem1
            // 
            this.команда10ToolStripMenuItem1.Name = "команда10ToolStripMenuItem1";
            this.команда10ToolStripMenuItem1.Size = new System.Drawing.Size(137, 22);
            this.команда10ToolStripMenuItem1.Text = "Команда 10";
            this.команда10ToolStripMenuItem1.Click += new System.EventHandler(this.команда10ToolStripMenuItem1_Click);
            // 
            // Shape
            // 
            this.Shape.Location = new System.Drawing.Point(463, 291);
            this.Shape.Name = "Shape";
            this.Shape.Size = new System.Drawing.Size(69, 32);
            this.Shape.TabIndex = 7;
            this.Shape.Text = "Shape";
            this.Shape.UseVisualStyleBackColor = true;
            this.Shape.Click += new System.EventHandler(this.Shape_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(326, 22);
            this.label2.TabIndex = 8;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 444);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Shape);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Страница Additional";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form2_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Button button1;
         private System.Windows.Forms.DataGridView dataGridView1;
         private System.Windows.Forms.Panel panel1;
         private System.Windows.Forms.CheckBox checkBox2;
         private System.Windows.Forms.CheckBox checkBox1;
         private System.Windows.Forms.Label label1;
         private System.Windows.Forms.MaskedTextBox maskedTextBox1;
         private System.Windows.Forms.Panel panel2;
         private System.Windows.Forms.Button button2;
         private System.Windows.Forms.VScrollBar vScrollBar1;
         private System.Windows.Forms.Button button3;
         private System.Windows.Forms.Button button4;
         private System.Windows.Forms.Button button5;
         private System.Windows.Forms.PictureBox pictureBox1;
         private System.Windows.Forms.MenuStrip menuStrip1;
         private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem Exit;
         private System.Windows.Forms.ToolStripMenuItem stringGridToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда1ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда2ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда3ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда4ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда5ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда6ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда7ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда8ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда9ToolStripMenuItem;
         private System.Windows.Forms.Button Shape;
         private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
         private System.Windows.Forms.ToolStripMenuItem команда1ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда2ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда3ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда4ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда5ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда6ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда7ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда8ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда9ToolStripMenuItem1;
         private System.Windows.Forms.ToolStripMenuItem команда10ToolStripMenuItem;
         private System.Windows.Forms.ToolStripMenuItem команда10ToolStripMenuItem1;
         private System.Windows.Forms.Label label2;
    }
}